%title: Infra Cloud Infomaniak
%author: xavki


██████╗  ██████╗ ███████╗████████╗ ██████╗ ██████╗ ███████╗███████╗ ██████╗ ██╗     
██╔══██╗██╔═══██╗██╔════╝╚══██╔══╝██╔════╝ ██╔══██╗██╔════╝██╔════╝██╔═══██╗██║     
██████╔╝██║   ██║███████╗   ██║   ██║  ███╗██████╔╝█████╗  ███████╗██║   ██║██║     
██╔═══╝ ██║   ██║╚════██║   ██║   ██║   ██║██╔══██╗██╔══╝  ╚════██║██║▄▄ ██║██║     
██║     ╚██████╔╝███████║   ██║   ╚██████╔╝██║  ██║███████╗███████║╚██████╔╝███████╗
╚═╝      ╚═════╝ ╚══════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝ ╚══▀▀═╝ ╚══════╝



-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : introduction

<br>

What is it ?

    * transactional & relationnal database

    * query language = SQL (Structured Query Language)

    * Client : psql

    * Many extensions (postgis, timescaledb...)

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : introduction

<br>

ACID

    * Atomicity : transaction must be done to the end

    * Consitency : if a fail of the instance, rollback to a consistent state

    * Isolation : transations are done in order

    * Durability : a change will be permanent


-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : introduction

<br>

Some definitions

  * Instance : 1 postgresql application in a server

  * Cluster : many instances connected to allow high availability (replication)

  * Failover : switch when an instance is failed 

  * Hot standby : 1 RW + others read

-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : introduction

<br>

Some definitions

  * Database : contains one or many tables, logical level

  * Table : structured data storage, logical level

  * Fields : column in a table with specific carateristics

  * Row : a line in a table with data splitted in fields

  * Schema : like a namespace in a database

  * Role or user


-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : introduction

<br>

  Cluster > Instance > Database > Schema > Table > Fields x Rows


-----------------------------------------------------------------------------------------------------------                                       

# Postgresql : introduction

<br>

In our project :

  * Mattermost install need a postgresql isntance
  
  * Postgresql cluster : streaming replication

  * Replication manager : to manage failover and replication

  * Consul : to provide a dedicated master dns

  * ... and backups (restic)

  * ... and monitoring (exporter)