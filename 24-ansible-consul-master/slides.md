%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Consul master

<br>

Plan :

  * create a new role

  * install prerequisites

  * create user, group and directory

  * check if consul exist

  * check consul version if exists

  * download consul and push it in /usr/local/cin

  * deploy conf and systemd service

  * start consul & restart on notify

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Consul master

<br>

Variables : 

```
consul_version: 1.18.0
consul_dir_data: /var/lib/consul
consul_dir_config: /etc/consul.d/
consul_binary_path: /usr/local/bin
consul_datacenter: xavki
consul_dns_domain: consul
consul_master: false
consul_group: meta-app_consul
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Consul master

<br>

Create role 

```
ansible galaxy init roles/consul
```

Install prerequisites

```
- name: install utils
  apt:
    name: curl,wget,unzip
    state: present
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Consul master

<br>

Create user and group

```
- name: create group consul
  group:
    name: consul
    system: yes
    state: present

- name: create user consul
  user:
    name: consul
    system: yes
    shell: /sbin/nologin
    state: present
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Consul master

<br>

Create directories

```
- name: create directory for consul agent data
  file:
    path: "{{ item }}"
    state: directory
    mode: 0750
    owner: consul
    group: consul
  loop:
  - "{{ consul_dir_data }}"
  - "{{ consul_dir_config }}" 
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Consul master

<br>

Check if consul exists and its version

```
- name: check if consul exists
  stat:
    path: "{{ consul_binary_path }}/consul"
  register: __consul_exists

- name: if consul exists get version
  shell: "cat /etc/systemd/system/consul.service | grep Version | sed s/'.*Version '//g"
  register: __get_consul_version
  when: __consul_exists.stat.exists == true
  changed_when: false
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Consul master

<br>

Download binary if no consul or consul needs to be upgrade

```
- name: download consul
  unarchive:
    src: https://releases.hashicorp.com/consul/{{ consul_version }}/consul_{{ consul_version }}_linux_amd64.zip
    dest: "{{ consul_binary_path }}"
    remote_src: yes
    mode: 0750
    owner: consul
    group: consul
  when: __consul_exists.stat.exists == False or not __get_consul_version.stdout == consul_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Consul master

<br>

Push conf files

```
- name: create service consul for agent     
  template:
    src: "{{ item.file }}.j2"          
    dest: "{{ item.path }}/{{ item.file }}"
    mode: 0750
    owner: "{{ item.user }}"
    group: "{{ item.user }}"
  loop:
  - { path: "/etc/systemd/system" ,file: "consul.service", user: "root" }
  - { path: "{{ consul_dir_config }}" ,file: "config.json", user: "consul" } 
  notify: reload_daemon_and_restart_consul
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Consul master

<br>

The systemd template

```
[Unit]
Description=Consul Service Discovery Agent Version {{ consul_version }}
Documentation=https://www.consul.io/
After=network-online.target
Wants=network-online.target

[Service]
Type=simple
User=consul
Group=consul
ExecStart={{ consul_binary_path }}/consul agent \
	-node={{ inventory_hostname }} \
	-config-dir={{ consul_dir_config }}

ExecReload=/bin/kill -HUP $MAINPID
KillSignal=SIGINT
TimeoutStopSec=5
Restart=on-failure
SyslogIdentifier=consul

[Install]
WantedBy=multi-user.target
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Consul master

<br>

The configuration template

```
{
    "advertise_addr": "{{ ansible_default_ipv4.address }}",
    "bind_addr": "{{ ansible_default_ipv4.address }}",
    "client_addr": "0.0.0.0",
    "datacenter": "{{ consul_datacenter }}",
    "data_dir": "{{ consul_dir_data }}",
    "domain": "{{ consul_dns_domain }}",
    "enable_script_checks": true,
    "dns_config": {
        "enable_truncate": true,
        "only_passing": true
    },
    "enable_syslog": true,
    "leave_on_terminate": true,
    "log_level": "INFO",
    {% if consul_master == true %}
    "server": true,
    "ui": true,
    "bootstrap_expect": {{ groups[consul_group] | length }},
    {% endif %}
    "rejoin_after_leave": true,
    "retry_join": [
    	"{{ groups[consul_group] | map('extract', hostvars, ['ansible_host']) | join('\",\"') }}"
    ]
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Consul master

<br>

And finally flush handler and ensure consul is started

```
- meta: flush_handlers

- name: start consul
  service:
    name: consul
    state: started
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Consul master

<br>

The handler

```
- name: reload_daemon_and_restart_consul
  systemd:
    name: consul
    state: restarted
    daemon_reload: yes
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Consul master

<br>

The playbook

```
- name: consul_master
  hosts: meta-app_consul
  become: true
  vars:
    consul_master: true
  roles:
    - consul
```