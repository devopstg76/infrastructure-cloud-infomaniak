%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 



-----------------------------------------------------------------------------------------------------------                                       

# Terraform - remote state with gitlab

<br>

Documentation : https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html

Playlist terraform : go to xavki home page

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - remote state with gitlab

<br>


```
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/<id_projet_gitlab>/terraform/state/<state_name>"
    lock_address = "https://gitlab.com/api/v4/projects/<id_projet_gitlab>/terraform/state/<state_name>/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/<id_projet_gitlab>/terraform/state/<state_name>/lock"
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_min = 5
  }
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - remote state with gitlab

<br>

Maybe an activation before ?

```
You must enable the Infrastructure menu for your project. Go to Settings > General, expand Visibility, project features, permissions, and under Infrastructure, turn on the toggle.
```

Project ID : in general settings (or init command)

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - remote state with gitlab

<br>

Generate a project token

  * scope API

Add these lines to your openrc.sh file

```
export TF_HTTP_USERNAME="yyyy"
export TF_HTTP_PASSWORD="xxxx"
```