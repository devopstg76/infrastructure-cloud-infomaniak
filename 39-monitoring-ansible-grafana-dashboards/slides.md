%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana Dashboards

<br>

Grafana

  * visualization

  * dashboards : large community

  * many datasources

  * covers all observability : metrics, logs and traces

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana Dashboards

<br>

Add some variables

```
grafana_prometheus_address: "vmetrics.service.xavki.consul:8428"
grafana_dashboards:
  - { label: "node-exporter", json: "https://raw.githubusercontent.com/rfrail3/grafana-dashboards/master/prometheus/node-exporter-full.json" }
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana Dashboards

<br>

Add datasource

```
- name: add victoriametrics datasource
  grafana_datasource:
    name: "VictoriaMetrics"
    grafana_url: "http://127.0.0.1:3000"
    grafana_user: "{{ grafana_admin_user }}"
    grafana_password: "{{ grafana_admin_password }}"
    org_id: "1"
    ds_type: "prometheus"
    ds_url: "http://{{ grafana_prometheus_address }}"
  changed_when: false
  ignore_errors: true
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana Dashboards

<br>

Iterate to create dashboards

```
- name: "Create dashboard - {{ item.label }}"
  include_tasks: create_dashboard.yml
  with_items: "{{ grafana_dashboards }}"
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana Dashboards

<br>

Download file

```
- name: "Install {{ item.label }} dashboard"
  get_url:
    url: "{{ item.json }}"
    dest: "/var/lib/grafana/{{ item.label }}.json"
    mode: '0755'
  notify: restart_grafana
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana Dashboards

<br>

Check to change datasource if needed

```
- name: change datasource
  replace: 
    path: "/var/lib/grafana/{{ item.label }}.json"
    regexp: '^(.*)\$\{DS_PROMETHEUS\}(.*)$'
    replace: '\1VictoriaMetrics\2'
    backup: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana Dashboards

<br>

Register dashboards

```
- name: "Activate dashboard for {{ item.label }}"
  template:
    src: dashboard-register.yml.j2
    dest: /etc/grafana/provisioning/dashboards/{{ item.label }}.yml
    mode: 0755
  notify: restart_grafana
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana Dashboards

<br>

The register template

```
apiVersion: 1
providers:
- name: '{{ item.label }}'
  orgId: 1
  folder: ''
  type: file
  disableDeletion: false
  updateIntervalSeconds: 30
  options:
    path: /var/lib/grafana/{{ item.label }}.json
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Grafana Dashboards

<br>

Add the handler

```
- name: restart_grafana
  systemd:
    name: grafana-server
    state: restarted
    enabled: yes
    daemon_reload: yes
```