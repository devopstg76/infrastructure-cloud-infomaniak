%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 



-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Introduction and Concepts

<br>

Playlist Ansible ~ 130 tutorials

<br>

How it works ?

	* use SSH

	* inventory : static or dynamic to declare our servers and infrastructure

	* roles : to compile tasks to install a dedicated stack (or part of stack)

	* playbook : on which element of the inventory do we want to play which role(s) ?

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Introduction and Concepts

<br>

Static inventory (declarative)

```
all:
  children:
    grp1:
      hosts:
        srv1:
        srv2:
  hosts:
    srv3
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Introduction and Concepts

<br>

Roles structure


```
roles/
├── base
│   ├── common-dns
│   ├── logrotate
│   ├── secu-custom
│   └── volumes
├── consul
│   ├── consul
│   └── consul_services
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Introduction and Concepts

<br>

Roles skeleton

```
consul/
├── defaults
├── handlers
├── meta
├── README.md
├── tasks
├── templates
├── tests
└── vars
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Introduction and Concepts

<br>

Test your connection

```
terraform state show openstack_networking_floatingip_v2.floatip_1
sudo apt install -y ansible
ansible -i "195.15.198.51," all -u debian -m ping
ansible -i "195.15.198.51," all -u debian -b  -m command -a 'ls -la /root/'
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Introduction and Concepts

<br>

Our inventory file : 00_inventory.yml

```
all:
  children:
    openvpn:
      hosts:
        srv1:
          ansible_host: 195.15.198.51
```

Test it

```
ansible -i 00_inventory.yml all -u debian -b  -m command -a 'ls -la /root/'
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Introduction and Concepts

<br>

Create a role

```
mkdir roles/
ansible-galaxy init roles/openvpn
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Introduction and Concepts

<br>

Add a simple task

```
- name: create a file in /root/
  file:
    path: /root/toto.txt
    state: touch
    owner: root
    group: root
    mode: 0750
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Introduction and Concepts

<br>

Playbook

```
- name: install openvpn
  hosts: openvpn
  become: true
  roles:
    - openvpn
``` 


<br>

Run the play

```
ansible-playbook -i 00_inventory.yml -u debian playbook.yml
```