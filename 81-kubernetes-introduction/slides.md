%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗ ███╗   ██╗███████╗████████╗███████╗███████╗
██║ ██╔╝██║   ██║██╔══██╗██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝██╔════╝██╔════╝
█████╔╝ ██║   ██║██████╔╝█████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   █████╗  ███████╗
██╔═██╗ ██║   ██║██╔══██╗██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   ██╔══╝  ╚════██║
██║  ██╗╚██████╔╝██████╔╝███████╗██║  ██║██║ ╚████║███████╗   ██║   ███████╗███████║
╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚══════╝╚══════╝




-----------------------------------------------------------------------------------------------------------

# Kubernetes : Introduction - what is it ??

<br>

Containers were pretty cool :

  but only a new package version with many improvements

kubernetes, what is it ? 

  * a container orchestrator
    * at the beginning with docker
    * now : containerd, crio...

  * manage a cluster of containers

It came from Google : Borg (internal orchestrator)

  Great video : John Wilkes - Cluster management at Google with Borg
  https://www.youtube.com/watch?v=wy3L7XUq-g0

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Introduction - what is it ??

<br>

Why to use it ?

<br>

  * scaling : to handle some various worloads (increase/decrease instances number)
    for example : an ecommerce website

<br>

  * autohealing : in the past (or not), apps could need to be restart
    or need to remove an instance of a loadbalancer when an instance respond an error

<br>

  * distribution : to improve the instance distribution on all your infrastructure
    for example : to improve the density in DC or cloud

<br>

  * deployments : to deploy more easily with a better process (ci/cd...)

<br>

  * abstraction : to add an abstraction layer to not care about hardware and OS

<br>

  * and by the way : to improve observability (and tracing)

-----------------------------------------------------------------------------------------------------------

# Kubernetes : Introduction - what is it ??

<br>