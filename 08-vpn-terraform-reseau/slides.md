%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 



-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Network Installation

<br>

Pre-requisites :

	* create a dedicated directory to our project

	* git init / git add / git commit

	* remote git repository on gitlab (or where do you want)

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Network Installation

<br>

	* .gitignore :

```
**/openrc.sh
**/.env
**/.envrc
**/.terraform/*
**/*.tfstate
**/*.tfstate.* 
**/*.tfvars
**/*.tfvars.json
**/.terraformrc
**/terraform.rc
**/*.md.swp
**/*.log
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Network Installation

<br>

Create a provider file

 * Doc : https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs

 * Versions : terraform + provider

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Network Installation

<br>

Create a provider file

```
terraform {
  required_version = ">= 0.14.0" #version de terraform
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.52.1" #version du provider
    }
  }
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Network Installation

<br>

Create a variable file - external network

Doc : https://gitlab.com/xavki/presentations-terraform-fr/-/blob/master/5-variables-definition/slides.md

Example tfvars : 

```
terraform plan -var-file=prod.tfvars
```

```
variable "network_external_id" {
  type 		= string
  default  	= "0f9c3806-bd21-490f-918d-4a6d1c648489"
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Network Installation

<br>

Create a variable file - internal network name & cidr

```
variable "network_internal_dev" {
  type 		= string
  default  	= "internal_dev"
}

variable "network_subnet_cidr" {
  type = string
  default = "10.0.1.0/24"
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Network Installation

<br>

Create a variable file - our key pair

```
variable "ssh_public_key_default_user" {
  type = string
  default = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINKkmKOi1M0P7zIG5SlCzCkC/DAmoO7CsGD2ANKtH5my oki@doki"
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Network Installation

<br>

Create a keypair file - add resource

  * this is not our final system user 
  
  * just the user that initiate our system (to run ansible)

```
resource "openstack_compute_keypair_v2" "ssh_public_key" {
  name     	= "default_key"
  public_key 	= var.ssh_public_key_default_user
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Network Installation

<br>

Create our network components - the router

```
resource "openstack_networking_router_v2" "rt1" {
  name 			= "rt1"
  admin_state_up 	= "true"
  external_network_id 	= var.network_external_id
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Network Installation

<br>

Create our network components - our network

```
resource "openstack_networking_network_v2" "network_name" {
  name = var.network_internal_dev
  admin_state_up = "true"
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Network Installation

<br>

Create our network components - our subnet

  * we reuse the network name for the subnet (be careful)

```
resource "openstack_networking_subnet_v2" "network_subnet" {
  name = var.network_internal_dev
  network_id = openstack_networking_network_v2.network_name.id
  cidr = var.network_subnet_cidr
  ip_version = 4
  enable_dhcp = true
  dns_nameservers = ["1.1.1.1","8.8.8.8"]
}
```

-----------------------------------------------------------------------------------------------------------                                          

# Terraform - Network Installation

<br>

Create our network components - associate the subnet to our router

```
resource "openstack_networking_router_interface_v2" "network_router_interface" {
  router_id = openstack_networking_router_v2.rt1.id
  subnet_id = openstack_networking_subnet_v2.network_subnet.id
}
```
