%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Introduction ??

<br>

What is it ?

    * reverse-proxy : between client and servers, poor handling of requests, tls...

    * load-balancer : distribute requests to manage workload on backend servers, high availability

    * api-gateway : interface between APIs and clients, many options to handle requests (rate limiting, authentication, rewriting requests, monitoring...)

    * cloud native applciation proxy

Github : https://github.com/traefik/traefik
Official Website : https://traefik.io/traefik/ 


-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Introduction ??

<br>

Main features

    * capability to use a provider for backend servers (API)
        ex : docker, consul, kubernetes, nomad, ecs, etcd, zookeeper, http...

    * capability to use a provider for dynamic configuration !!

    * directly manage tls certs with let's encrypt (challenges, renew...)

    * hot reload

    * GUI

-----------------------------------------------------------------------------------------------------------                                       

# Traefik - Introduction ??

<br>

Core concepts

    * entrypoints : where the trafic come from
        * [host]:port[/tcp|/udp]
        * ports, redirection, tls, protocol

    * routers : application of rules on incoming or outgoing trafic

    * middlewares : technical rules to handle requests or datas
        * rewrite url, basic auth, compress, retry, rate limit, white listening...
        * can be chained
        * tcp and http middlewares
        * https://doc.traefik.io/traefik/middlewares/http/overview/

    * services : backends where the trafic is sent
        * load balancing, round robin, weighted, healthcheck, mirroring