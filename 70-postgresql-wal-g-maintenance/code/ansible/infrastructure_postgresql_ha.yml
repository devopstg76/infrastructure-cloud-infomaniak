- name: install our postgresql cluster
  become: yes
  hosts: meta-app_postgresql
  roles:
    - volumes
    - databases/postgresql/postgresql_simple
    - monitoring/postgresql_exporter
    - consul/consul_services
  vars:
    volumes_disks:
      - {disk: '/dev/sdb',path: '/data', owner: "root"}
    postgresql_simple_data_dir: /data/postgresql
    postgresql_simple_pghba_enabled: false
    consul_services:
      - {
        name: "postgresql",
        type: "tcp",
        check_target: "127.0.0.1:5432",
        interval: "10s",
        port: 5432      
        }
      - {
        name: "postgresql_exporter",
        type: "tcp",
        check_target: "127.0.0.1:9187",
        interval: "10s",
        port: 9187      
        }

- name: install our postgresql replication
  become: yes
  hosts: postgresql1,postgresql2
  serial: 1
  roles:
    - databases/postgresql/postgresql_replication
    - consul/consul_services
    - monitoring/custom_metrics
  vars:
    postgresql_replication_data_dir: "/data/postgresql"
    postgresql_replication_primary: "postgresql1"
    postgresql_replication_group: "meta-app_postgresql"
    postgresql_replication_pghba_entries:
      - { host: 'host', database: 'mattermost', user: 'all', address: '10.0.1.0/24', method: 'md5' }

    consul_services:
      - {
        name: "postgresql_primary",
        type: "args",
        check_target: "/opt/failover_postgresql.sh",
        interval: "10s",
        port: 5432
        }

    postgresql_replication_wal_g_enabled: true
    postgresql_replication_wal_g_s3_key: "{{ backups_s3_secret_key }}"
    postgresql_replication_wal_g_s3_secret_key: "{{ vault_backups_consul_secret_key }}"
    postgresql_replication_wal_g_s3_endpoint: "https://s3.pub1.infomaniak.cloud/"
    postgresql_replication_wal_g_s3_bucket: "s3://wal-g"

- name: postgresql management
  become: yes
  hosts: postgresql1
  roles:
    - databases/postgresql/postgresql_manage
  vars:
    postgresql_manage:
      users:
        - { name: "xavki_mattermost", password: "{{ vault_user_xavki_password }}" }
      db_name:
        - mattermost
      privileges:
        - { db: "mattermost" , type: "database", user: "xavki_mattermost", permissions: "ALL" }
      privileges_schema:
        - { schemas_name: "public", owner: "xavki_mattermost", db: "mattermost" }
    vault_user_xavki_password: !vault |
      $ANSIBLE_VAULT;1.1;AES256
      38646664303964666361613266303563313139393630623435333462363762653839343738396239
      3137663338633963366139633238356336343338393862340a353237656335323233646461333635
      65626564336136306335383863666130643239653363623461626533306661373561333535656461
      6265353731313665630a626639383536393163326564373561366465386330636661623462373835
      65396237376365613465623531303262653638663934353339636538393230633165


- name: install our postgresql backup
  become: yes
  hosts: postgresql2
  roles:
    - databases/postgresql/postgresql_backup
    - monitoring/custom_metrics
  vars:
    backups_s3_key: "{{ backups_s3_secret_key }}"
    backups_secret_key: "{{ vault_backups_consul_secret_key }}"
    backups_secret_password: "{{ vault_backups_consul_secret_password }}"

    vault_backups_consul_secret_password: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          35336537663831383630373439333735613134613962316533653932386230383837633764356131
          3835336237666266663261356536646462656338366431310a633130616161303466303533643436
          66303539353837343439646537396566333437663264376664343361636161643336666661336566
          6431623632323236340a326537383333626438343164363439663333343239356536643163643063
          30356330616363363533313665396136633761623463303561356665383533393466356235363132
          6536613966653466386361363965653431613761623734643034