---
# tasks file for ansible/roles/monitoring/blackbox_exporter
- name: create group blackbox_exporter
  ansible.builtin.group:
    name: "{{ blackbox_exporter_user }}"
    system: yes
    state: present

- name: create user blackbox_exporter
  ansible.builtin.user:
    name: "{{ blackbox_exporter_user }}"
    system: yes
    shell: /sbin/nologin
    state: present

- name: check if blackbox_exporter exists
  ansible.builtin.stat:
    path: "{{ blackbox_exporter_binary_path }}/blackbox_exporter"
  register: __blackbox_exporter_exists

- name: if blackbox_exporter exists get version
  ansible.builtin.shell: "cat /etc/systemd/system/blackbox_exporter.service | grep Version | sed s/'.*Version '//g"
  register: __blackbox_exporter_get_version
  when: __blackbox_exporter_exists.stat.exists == true
  changed_when: false

- name: download blackbox_exporter
  ansible.builtin.unarchive:
    src: "https://github.com/prometheus/blackbox_exporter/releases/download/v{{ blackbox_exporter_version }}/blackbox_exporter-{{ blackbox_exporter_version }}.linux-amd64.tar.gz"  
    dest: "/tmp/"
    remote_src: yes
    mode: 0750
    owner: "{{ blackbox_exporter_user }}"
    group: "{{ blackbox_exporter_user }}"
  when: __blackbox_exporter_exists.stat.exists == False or not __blackbox_exporter_get_version.stdout == blackbox_exporter_version


- name: move the binary to the final destination
  copy:
    src: "/tmp/blackbox_exporter-{{ blackbox_exporter_version }}.linux-amd64/blackbox_exporter"
    dest: "{{ blackbox_exporter_binary_path }}"
    mode: 0750
    remote_src: yes
    owner: "{{ blackbox_exporter_user }}"
    group: "{{ blackbox_exporter_user }}"
  when: __blackbox_exporter_exists.stat.exists == False or not __blackbox_exporter_get_version.stdout == blackbox_exporter_version

- name: clean
  file:
    path: "/tmp/blackbox_exporter-{{ blackbox_exporter_version }}.linux-amd64/"
    state: absent
  when: __blackbox_exporter_exists.stat.exists == False or not __blackbox_exporter_get_version.stdout == blackbox_exporter_version


- name: add systemd service for blackbox_exporter
  template:
    src: blackbox_exporter.service.j2
    dest: "/etc/systemd/system/blackbox_exporter.service"
    owner: root
    group: root
    mode: 0750
  notify: restart_blackbox_exporter

- name: add blackbox_exporter configuration
  template:
    src: blackbox.yml.j2
    dest: "/etc/blackbox.yml"
    owner: "{{ blackbox_exporter_user }}"
    group: "{{ blackbox_exporter_user }}"
    mode: 0750
  notify: restart_blackbox_exporter

- name: start blackbox_exporter
  service:
    name: blackbox_exporter
    state: started
    enabled: yes
