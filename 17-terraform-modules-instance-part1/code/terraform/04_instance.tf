
module "openvpn" {
  source                      = "./modules/instance"
  instance_name               = "openvpn"
  instance_key_pair           = openstack_compute_keypair_v2.ssh_public_key.name
  instance_security_groups    = [openstack_networking_secgroup_v2.openvpn.name, openstack_networking_secgroup_v2.ssh.name, "default"]
  instance_network_internal   = var.network_internal_dev
  instance_network_external_name = var.network_external_name
  instance_network_external_id  = var.network_external_id
  metadatas                   = {
    environment          = "dev"
  }
  depends_on = [module.network_dev,openstack_networking_secgroup_rule_v2.secgroup_openvpn_rule_v4]
}
