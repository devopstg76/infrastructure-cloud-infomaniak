#!/usr/bin/bash


pip3 install openstacksdk python-openstackclient

source openrc.sh

openstack keypair create --public-key ~/.ssh/info.pub keypair_xavki

openstack server create --key-name keypair_xavki --flavor a1-ram2-disk20-perf1 --network ext-net1 --image "Debian 12 bookworm" myvm

openstack security group rule create --ingress --protocol tcp --dst-port 22 --ethertype IPv4 default

openstack project list
openstack server list
openstack server show xxx
openstack server delete xxx
openstack keypair delete xxx

